# Copyright (C) 2021 Matteo Ghetta matteo dot ghetta at gmail dot com
# 
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this.  If not, see <http://www.gnu.org/licenses/>.

import requests
from bs4 import BeautifulSoup
import pandas as pd
import plotly.express as px

def scrape_data(url):
    """[summary]

    Args:
        url ([str]): complete url of the qgis changelog file

    Returns:
        [fig]: plotly Figure object. The fig can be shown using fig.show()
        [df]: pandas DataFrame. Useful to get the raw data
    """
    
    url = url
    page = requests.get(url)

    soup = BeautifulSoup(page.content, 'html.parser')
    soup_lxml = BeautifulSoup(page.content, 'lxml')

    # categories list with the id
    raw_categories = soup.find(id='category-list')

    # dict with key = category name and value = category html id
    category_dict = {}
    for i in raw_categories.find_all('a', href=True):
        category_dict[i.text.strip()] = i['href'][1:]

    # cetegory to skip (used later)
    category_tags = soup.find_all("a", {"class": "anchor"})

    # initialize empty dict that will ke filled with
    # key = category and value = [list of feature for each category]
    d = {}


    for k, v in category_dict.items():
        # find the tag of each category
        tag = soup.find("a", {"id": v}).find_next()
        # skip the anchor class
        while tag not in category_tags:
            # iterable of each tag
            tag = tag.find_next()
            if tag is None: 
                break
            # the h3 tag contains the text of the category added
            if tag.name == "h3":
                if d.get(k): 
                    d[k].append(tag.text)
                else: 
                    d[k] = [tag.text]

    l = []
    for k, v in d.items():
        l.append(
            [
                k, len(v)
            ]
        )
    
    # parse the version of QGIS from the URL
    url_parsed = url.split('/')
    version = None
    if url_parsed[-1] == '':
        version = url_parsed[-2]
    else:
        version = url_parsed[-1]

    df = pd.DataFrame(data=l, index=None, columns=['category', version])

    fig = px.bar(df, 'category', version ,text=version, title=f'QGIS {version}')
    
    return fig, df

