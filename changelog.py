# Copyright (C) 2021 Matteo Ghetta matteo dot ghetta at gmail dot com
# 
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this.  If not, see <http://www.gnu.org/licenses/>.

from utils import scrape_data
from functools import reduce
import plotly.express as px
import pandas as pd


# QGIS 3.0
# url_3_0 = 'https://changelog.qgis.org/en/qgis/version/3.0.0/'
# fig_3_0, df_3_0 = scrape_data(url_3_0)
# # fig_3_0.show()

# # QGIS 3.2
# url_3_2 = 'https://changelog.qgis.org/en/qgis/version/3.2.0/'
# fig_3_2, df_3_2 = scrape_data(url_3_2)
# # fig_3_2.show()

# # QGIS 3.4 LTR
# url_3_4 = 'https://changelog.qgis.org/en/qgis/version/3.4-LTR/'
# fig_3_4, df_3_4 = scrape_data(url_3_4)
# # fig_3_4.show()

# # QGIS 3.6
# url_3_6 = 'https://changelog.qgis.org/en/qgis/version/3.6.0'
# fig_3_6, df_3_6 = scrape_data(url_3_6)
# # fig_3_6.show()

# # QGIS 3.8
# url_3_8 = 'https://changelog.qgis.org/en/qgis/version/3.8'
# fig_3_8, df_3_8 = scrape_data(url_3_8)
# # fig_3_8.show()

# # QGIS 3.10
# url_3_10 = 'https://changelog.qgis.org/en/qgis/version/3.10/'
# fig_310, df_310 = scrape_data(url_3_10)
# # fig_310.show()

# # QGIS 3.12
# url_3_12 = 'https://changelog.qgis.org/en/qgis/version/3.12/'
# fig_312, df_312 = scrape_data(url_3_12)
# fig_312.show()

# QGIS 3.14
url_3_14 = 'https://changelog.qgis.org/en/qgis/version/3.14/'
fig_314, df_314 = scrape_data(url_3_14)
fig_314.layout.font.size = 20
fig_314.show()

# QGIS 3.16
url_3_16 = 'https://changelog.qgis.org/en/qgis/version/3.16/'
fig_316, df_316 = scrape_data(url_3_16)
fig_316.layout.font.size = 20
fig_316.show()

# QGIS 3.18
url_3_18 = 'https://changelog.qgis.org/en/qgis/version/3.18'
fig_318, df_318 = scrape_data(url_3_18)
fig_318.layout.font.size = 20
fig_318.show()

# QGIS 3.20
url_3_20 = 'https://changelog.qgis.org/en/qgis/version/3.20'
fig_320, df_320 = scrape_data(url_3_20)
fig_320.layout.font.size = 20
fig_320.show()

# create a list of the df of each version
df_list = [
    #df_3_0,
    #df_3_2,
    #df_3_4,
    #df_3_6,
    #df_3_8,
    #df_310, 
    #df_312, 
    df_314, 
    df_316, 
    df_318,
    df_320
]

# merge all the df in a single one with how=outer to keep also the NaN
df_merged = reduce(lambda  left,right: pd.merge(left,right,on=['category'],
                                            how='outer'), df_list)

# create a melted dataframe (used to create a bar chart with all the versions)
df_melted = pd.melt(df_merged, id_vars="category")


# bar plot per categorie
df_cat = df_melted.groupby('category')['value'].sum()
df_cat = df_cat.to_frame()
fig_cat = px.bar(df_cat, df_cat.index, 'value')
fig_cat.layout.xaxis.title.text = None
fig_cat.layout.yaxis.title.text = None
fig_cat.layout.title = 'Categorie'
fig_cat.layout.font.size = 20
fig_cat.show()
# fig_cat.write_html('/home/matteo/features.html', include_plotlyjs='cdn')

# suddivisione categorie per ogni version
fig_melted = px.bar(df_melted, x="category", y="value", color="variable")
fig_melted.layout.xaxis.title.text = None
fig_melted.layout.yaxis.title.text = 'Total Features'
fig_melted.layout.font.size = 20
fig_melted.show()
# fig_melted.write_html('/home/matteo/features.html', include_plotlyjs='cdn')

# somm delle features per ogni version
total_features = df_merged.sum().to_frame()
# rimuovi prima inutile riga
total_features = total_features.iloc[1:]
total_features.rename(columns={total_features.columns[0]: "Total" }, inplace = True)

# plot
total_fig = px.bar(total_features, total_features.index, 'Total')
total_fig.layout.xaxis.title.text = 'Version'
total_fig.layout.yaxis.title.text = 'Total Features'
total_fig.data[0].marker.color = 'green'
total_fig.update_xaxes(type='category')
total_fig.layout.font.size = 20
# total_fig.write_html('/home/matteo/features.html', include_plotlyjs='cdn')
