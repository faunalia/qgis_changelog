# QGIS Changelog data scraper

The data are scraped from the official [changelog website](https://changelog.qgis.org/en/qgis/version/list/)
and the data are converted into `pandas` DataFrame(s) and `plotly.express` bar
chart(s) are build.

## How is it working?

It is strongly suggested to create a python virtual environment and activate it:

```shell
python3 -m venv my_venv

source my_venv/bin/activate
```

The `pip install` the packages required from the `requirements.txt` file:

```shell
pip install -r requirements.txt
```

Once all the dependencies are installed you can just run the `changelog.py` file:

```shell
python3 -m changelog.py
```

And the images will be opened in your Web Browser.
